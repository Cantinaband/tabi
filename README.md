# Tabi

Tabi is react PWA written in typescript. It helps you to find nice places on your trip around the world.

## Installation

Use the npm.

```bash
npm install
```

## Usage

```bash
npm start
```

Runs the app in the development mode with HTTPS.
Open [https://localhost:3000](https://localhost:3000) to view it in the browser.

## ☄️ Contributing

Feel free to open pull requests 🎉 For major changes, please open an issue first to discuss what you would like to change 🤹‍

## License

[MIT](https://choosealicense.com/licenses/mit/)
