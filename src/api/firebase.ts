import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";

const config = {
    apiKey: process.env.FIREBASE_API,
    authDomain: "tabi-be2f5.firebaseapp.com",
    databaseURL: "https://tabi-be2f5.firebaseio.com",
    projectId: "tabi-be2f5",
    storageBucket: "tabi-be2f5.appspot.com",
    messagingSenderId: "177898921502",
    appId: "1:177898921502:web:473a7ed2868d7a02d2180f"
};
firebase.initializeApp(config);
export default firebase;
