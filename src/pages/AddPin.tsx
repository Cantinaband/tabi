import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Formik, Field } from "formik";
import { Coords } from "google-map-react";
import { FormEvent } from "react";
import firebase from "../api/firebase";
import Geohash from "latlon-geohash";
import Loading from "../components/Loading";
import MapFilters from "../components/MapFilter";
import { FilterType, getFilters } from "../icons/filters/FilterIcon";

const Container = styled.div`
    display: flex;
    height: 100%;
    flex-direction: column;
    overflow: auto;
`;
const LoadingOverlay = styled.div`
    position: absolute;
    display: flex;
    justify-content: center;
    align-items: center;
    top: -10vh;
    left: 0;
    bottom: 0;
    right: 0;
    background: #ffb3ba60;
`;

const Form = styled.form`
    display: flex;
    flex: 1 0 auto;
    flex-direction: column;
    padding: 5vw;
    & > :nth-last-child(1) {
        margin-top: auto;
    }
`;

const Input = styled.input<{ isValid: boolean }>`
    outline: none;
    border: none;
    font-size: 1.25em;
    padding: 1%;
    border-radius: 0;
    margin: 1% 0;
    color: #686868;
    background-color: #ffffff95;
    border-bottom: 2px solid #ffdfba;
    ${(p: { isValid: boolean }) => p.isValid && "border-bottom-color: #baffc9"}
    -webkit-user-select: auto; /* Safari 3.1+ */
    user-select: auto;
    ::placeholder {
        color: #00000030;
    }
    :focus {
        border-bottom-color: #bae1ff;
        background-color: #ffdfba;
    }
`;

const TextArea = styled.textarea<{ isValid: boolean }>`
    -webkit-user-select: auto; /* Safari 3.1+ */
    user-select: auto;
    outline: none;
    border: none;
    border-radius: 0;
    font-size: 1.25em;
    padding: 1%;
    margin: 1% 0;
    color: #686868;
    background-color: #ffffff95;
    border-bottom: 2px solid #ffdfba;
    ${(p: { isValid: boolean }) => p.isValid && "border-bottom-color: #baffc9"}
    ::placeholder {
        color: #00000030;
    }
    :focus {
        border-bottom-color: #bae1ff;
        background-color: #ffdfba;
    }
    resize: none;
`;

const FormError = styled.div`
    color: red;
    font-size: 0.75em;
`;

const FormButton = styled.button<{ color?: string }>`
    outline: none;
    border: none;
    font-size: 1.25em;
    padding: 1%;
    margin: 5% 0;
    color: #fff;
    background-color: ${(p: { color?: string }) =>
        p.color ? p.color : "#baffc9"};
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.1);
    overflow: hidden;
    position: relative;
    :active {
        transform: scale(0.98);
    }
`;

const ImageButton = styled(FormButton)`
    background: #bae1ff;
    margin: 1% 0;
    input {
        cursor: inherit;
        display: block;
        font-size: 999px;
        filter: alpha(opacity=0);
        min-height: 100%;
        min-width: 100%;
        opacity: 0;
        position: absolute;
        right: 0;
        text-align: right;
        top: 0;
    }
`;

const Images = styled.div`
    display: grid;
    margin: 2% 0;
    grid-template-columns: repeat(auto-fill, minmax(100px, 1fr));
    grid-gap: 1em;
`;

const Filters = styled(MapFilters)`
    grid-area: filters;
`;

const Image = styled.img`
    width: 100%;
    height: auto;
`;
interface IAddPin {
    setLocationMode: (state: boolean) => void;
    currentCenter?: Coords;
    setAddPinOpen: (_: boolean) => void;
}

interface IAddPinForm {
    title: string;
    description: string;
    filters?: FilterType[];
    images: File[];
    location?: Coords;
}
export default function AddPin(props: IAddPin) {
    const [currentPosition, setCurrentPosition] = useState<Coords>();
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        if (!navigator.geolocation) {
            alert("Geolocation is not supported by your browser");
        } else {
            navigator.geolocation.getCurrentPosition(
                (r: Position) => {
                    setCurrentPosition({
                        lat: r.coords.latitude,
                        lng: r.coords.longitude
                    });
                },
                (error: PositionError) => console.log(error)
            );
        }
    }, []);
    return (
        <Container>
            <Formik
                initialValues={{
                    title: "",
                    description: "",
                    images: [] as File[],
                    location: undefined
                }}
                validate={(values: IAddPinForm) => {
                    let errors = {} as {
                        title: string;
                        description: string;
                        filters: string;
                        location: string;
                    };
                    if (!values.title) {
                        errors.title = "Title is required";
                    } else if (values.title.length < 2) {
                        errors.title = "Title is too short";
                    }
                    if (!values.description) {
                        errors.description = "Description is required";
                    } else if (values.description.length < 10) {
                        errors.description = "Description is too short";
                    }
                    if (
                        !values.filters ||
                        (values.filters && !values.filters.length)
                    ) {
                        errors.filters = "Filters are required";
                    }
                    if (!currentPosition) {
                        if (!props.currentCenter) {
                            errors.location = "Location is required";
                        }
                    }

                    return errors;
                }}
                onSubmit={async (values, { setSubmitting, resetForm }) => {
                    setLoading(true);
                    const images = Array.from(values.images).map(async i => {
                        const storage = firebase
                            .storage()
                            .ref()
                            .child(`images/${i.name}`);
                        return await storage
                            .put(i)
                            .then(() =>
                                storage.getDownloadURL().then(url => url)
                            );
                    });
                    Promise.all(images).then((imgs: string[]) => {
                        firebase
                            .firestore()
                            .collection("Pins")
                            .add({
                                title: values.title,
                                description: values.description,
                                images: imgs,
                                filters: values.filters,
                                geopoint: currentPosition
                                    ? new firebase.firestore.GeoPoint(
                                          currentPosition.lat,
                                          currentPosition.lng
                                      )
                                    : props.currentCenter &&
                                      new firebase.firestore.GeoPoint(
                                          props.currentCenter.lat,
                                          props.currentCenter.lng
                                      ),
                                geohash: currentPosition
                                    ? Geohash.encode(
                                          currentPosition.lat,
                                          currentPosition.lng,
                                          12
                                      )
                                    : props.currentCenter &&
                                      Geohash.encode(
                                          props.currentCenter.lat,
                                          props.currentCenter.lng,
                                          12
                                      )
                            })
                            .then(() => {
                                setLoading(false);
                                props.setAddPinOpen(false);
                                resetForm();
                                setSubmitting(false);
                            });
                    });
                }}
            >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    setFieldValue
                }) => (
                    <Form onSubmit={handleSubmit}>
                        <Input
                            type="text"
                            autoComplete="off"
                            placeholder="enter title..."
                            name="title"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.title}
                            isValid={
                                !errors.title && touched.title ? true : false
                            }
                        />
                        <FormError>
                            {errors.title && touched.title && errors.title}
                        </FormError>
                        <Field
                            render={() => (
                                <TextArea
                                    name="description"
                                    placeholder="enter description text..."
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.description}
                                    rows={3}
                                    isValid={
                                        !errors.description &&
                                        touched.description
                                            ? true
                                            : false
                                    }
                                ></TextArea>
                            )}
                        />
                        <FormError>
                            {errors.description &&
                                touched.description &&
                                errors.description}
                        </FormError>
                        <Filters
                            filters={getFilters()}
                            onChange={(filters: FilterType[]) => {
                                if (filters.length) {
                                    touched.filters = true;
                                }
                                setFieldValue("filters", filters);
                            }}
                        ></Filters>

                        <FormError>
                            {errors.filters &&
                                touched.filters &&
                                errors.filters}
                        </FormError>
                        {!currentPosition && !props.currentCenter && (
                            <>
                                <FormButton
                                    color={"#ffdfba"}
                                    name="location"
                                    type="button"
                                    onBlur={handleBlur}
                                    onClick={() => {
                                        props.setAddPinOpen(false);
                                        props.setLocationMode(true);
                                    }}
                                >
                                    set location
                                </FormButton>
                                <FormError>
                                    {errors.location &&
                                        !props.currentCenter &&
                                        touched.location &&
                                        errors.location}
                                </FormError>
                            </>
                        )}
                        <ImageButton>
                            add images
                            <input
                                onChange={(
                                    event: FormEvent<HTMLInputElement>
                                ) => {
                                    setFieldValue(
                                        "images",
                                        event.currentTarget.files
                                    );
                                }}
                                type="file"
                                name="images"
                                accept="image/png, image/jpeg"
                                multiple
                            ></input>
                        </ImageButton>
                        <Images>
                            {Array.from(values.images).map((file, i) => (
                                <Image
                                    key={i}
                                    src={URL.createObjectURL(file)}
                                />
                            ))}
                        </Images>
                        <FormButton type="submit" disabled={isSubmitting}>
                            add pin
                        </FormButton>
                    </Form>
                )}
            </Formik>
            {loading && (
                <LoadingOverlay>
                    <Loading></Loading>
                </LoadingOverlay>
            )}
        </Container>
    );
}
