import React, { useState, Suspense } from "react";
import Container from "../common/Container";
import Header from "../components/Header";
import Modal from "../components/Modal";
import AddPin from "./AddPin";
import { Coords } from "google-map-react";
import MarkerModal from "../components/MarkerModal";
import { IPin } from "../components/Map";

const Map = React.lazy(() => import("../components/Map"));

export default function Home() {
    const [addPinOpen, setAddPinOpen] = useState(false);
    const [markerModalOpen, setMarkerModalOpen] = useState(false);
    const [locationMode, setLocationMode] = useState(false);
    const [currentCenter, setCurrentCenter] = useState<Coords>();
    const [visibleMarkerData, setVisibleMarkerData] = useState<IPin | null>(
        null
    );
    return (
        <Container>
            <Header
                setMarkerModalOpen={setMarkerModalOpen}
                setAddPinOpen={setAddPinOpen}
                modalOpen={addPinOpen || markerModalOpen}
            ></Header>
            <Suspense fallback={<div>fallback Map</div>}>
                <Map
                    locationMode={locationMode}
                    setLocationMode={setLocationMode}
                    isMarkerModalOpen={markerModalOpen}
                    setMarkerModalOpen={setMarkerModalOpen}
                    setAddPinOpen={setAddPinOpen}
                    setCurrentCenter={setCurrentCenter}
                    setVisibleMarkerData={setVisibleMarkerData}
                />
            </Suspense>
            {visibleMarkerData && (
                <MarkerModal
                    setMarkerModalOpen={setMarkerModalOpen}
                    marker={visibleMarkerData}
                    isOpen={markerModalOpen}
                ></MarkerModal>
            )}

            <Modal open={addPinOpen}>
                <AddPin
                    setAddPinOpen={setAddPinOpen}
                    currentCenter={currentCenter}
                    setLocationMode={setLocationMode}
                ></AddPin>
            </Modal>
        </Container>
    );
}
