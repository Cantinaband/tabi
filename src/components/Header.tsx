import React from "react";
import styled from "styled-components";
import { ModalOpen } from "../common/icons";

const Container = styled.div`
    height: 10vh;
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    padding: 0 5vw;
    background: #ffdfba;
    z-index: 1;
    top: 0;
    left: 0;
    right: 0;
`;

interface IAddButton {
    isCloseButton: boolean;
}

const AddButton = styled.div<IAddButton>`
    height: 7vh;
    width: 7vh;
    color: ${(p: IAddButton) => (p.isCloseButton ? "#ffb3ba" : "#baffc9")};
    transition: all 150ms;
    transform: rotate(${(p: IAddButton) => (p.isCloseButton ? 0 : 45)}deg);
    :active {
        transform: scale(0.9)
            rotate(${(p: IAddButton) => (p.isCloseButton ? 0 : 45)}deg);
    }
`;
export default function Header(props: {
    setAddPinOpen: (_: boolean) => void;
    setMarkerModalOpen: (_: boolean) => void;
    modalOpen: boolean;
}) {
    return (
        <Container>
            <AddButton
                isCloseButton={props.modalOpen}
                onClick={() => {
                    props.setMarkerModalOpen(false);
                    props.setAddPinOpen(!props.modalOpen);
                }}
            >
                <ModalOpen></ModalOpen>
            </AddButton>
        </Container>
    );
}
