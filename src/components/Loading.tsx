import React from "react";
import styled from "styled-components";

const Wave = styled.div`
    position: relative;
    height: 80px;
    width: 180px;
`;

const Dot = styled.div`
    transform-origin: 50% 50%;
    height: 30px;
    width: 30px;
    border-radius: 50%;
    top: 0;
    position: absolute;
    animation: vertical-movement 1.1s infinite ease-in-out;
    &:nth-child(1) {
        left: 36px;
        background: #ffb3ba;
        animation-delay: -0.3s;
    }
    &:nth-child(2) {
        left: 72px;
        animation-delay: -0.6s;
        background: #bae1ff;
    }
    &:nth-child(3) {
        left: 108px;
        animation-delay: -0.9s;
        background: #baffc9;
    }
    &:nth-child(4) {
        left: 144px;
        animation-delay: -1.2s;
        background: #ffffba;
    }
    @keyframes vertical-movement {
        0%,
        100% {
            transform: translateY(0%);
        }
        50% {
            transform: translateY(50px);
        }
    }
`;

export default function Loading() {
    return (
        <Wave>
            <Dot></Dot>
            <Dot></Dot>
            <Dot></Dot>
            <Dot></Dot>
        </Wave>
    );
}
