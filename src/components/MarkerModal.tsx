import React, { useState } from "react";
import styled from "styled-components";
import { IPin } from "./Map";
import FilterIcon from "../icons/filters/FilterIcon";

const Modal = styled.div`
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    background: #ffffba;
    z-index: 2;
    min-height: 90vh;
`;

const Grid = styled.div`
    display: grid;
    grid-template-columns: repeat(9, 1fr);
    grid-template-rows: 3vh auto auto 1fr 1fr;
    height: 100%;
    grid-template-areas:
        ". . . . open . . . ."
        ". title title title title title title title ."
        ". description description description description description description description ."
        "images images images images images images images images images"
        ". . . . . . . . .";
`;

const OpenContainer = styled.div`
    grid-area: open;
    display: flex;
    align-items: center;
`;

const Open = styled.div`
    flex: 1;
    height: 4px;
    border-radius: 2px;
    background: #ffb3ba;
`;

const Row = styled.div`
    display: flex;
    align-items: center;
    height: 7vh;
    grid-area: title;
    margin-bottom: 2vh;
    & > svg {
        margin-right: 2vh;
        width: 7vh;
    }
`;

const Title = styled.h2`
    color: #bae1ff;
    margin: 0;
`;

const Description = styled.p`
    color: #ffb3ba;
    grid-area: description;
    font-size: 1em;
    font-weight: 800;
    margin-bottom: 5vh;
`;

const Images = styled.div`
    display: flex;
    flex-direction: row;
    padding: 0 calc(100vw / 9);
    flex-wrap: nowrap;
    grid-area: images;
    overflow-x: scroll;
    -webkit-overflow-scrolling: touch;
    ::-webkit-scrollbar {
        display: none;
    }
    &:after {
        display: flex;
        flex: 0 0 1px;
        content: "";
    }
`;

const Image = styled.img`
    margin-right: 3vh;
    object-fit: cover;
    max-height: 40vh;
    max-width: 70vw;
`;

interface IProps {
    setMarkerModalOpen: (_: boolean) => void;
    marker: IPin;
    isOpen: boolean;
}
export default function MarkerModal(props: IProps) {
    const [isOpen, setOpen] = useState(false);
    const [startY, setStartY] = useState(0);
    const [distance, setDistance] = useState(0);
    const { marker } = props;

    const getTransform = () => {
        if (!isOpen || !props.isOpen) {
            return `translate3d(0, calc(85vh - ${distance}px), 0)`;
        }
        return `translate3d(0, calc(10vh - ${distance}px), 0)`;
    };

    return (
        <Modal
            onTouchStart={s => {
                setStartY(s.nativeEvent.touches[0].clientY);
            }}
            onTouchMove={m => {
                m.stopPropagation();
                const d = startY - m.nativeEvent.touches[0].clientY;
                const area = window.innerHeight * 0.75;
                if (!isOpen && d <= area && d > 0) setDistance(d);
                if (isOpen && d < 0 && d >= -area) setDistance(d);
            }}
            onTouchEnd={() => {
                if (distance > 42 && !isOpen) {
                    setDistance(0);
                    props.setMarkerModalOpen(true);
                    setOpen(true);
                } else if (distance < -42 && isOpen) {
                    setDistance(0);
                    setOpen(false);
                    props.setMarkerModalOpen(false);
                } else {
                    setDistance(0);
                }
            }}
            style={{ transform: getTransform() }}
        >
            <Grid>
                <OpenContainer>
                    <Open></Open>
                </OpenContainer>
                <Row>
                    {marker.filters && (
                        <FilterIcon filter={marker.filters[0]}></FilterIcon>
                    )}
                    <Title>{marker.title}</Title>
                </Row>
                <Description>{marker.description}</Description>
                <Images>
                    {marker.images.map((image, i) => (
                        <Image key={i} src={image} />
                    ))}
                </Images>
            </Grid>
        </Modal>
    );
}
