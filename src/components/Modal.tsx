import React, { ReactNode } from "react";
import styled from "styled-components";

const Container = styled.div<{ open: boolean }>`
    position: absolute;
    top: 10vh;
    bottom: 0;
    right: 0;
    left: 0;
    background: #ffffba;
    z-index: 3;
    transition: all 400ms ease-out;
    transform: ${(p: { open: boolean }) =>
        "translate3d(0," + (p.open ? 0 : 100) + "%,0)"};
`;

interface IModal {
    open: boolean;
    children: ReactNode[] | ReactNode;
}
export default function Modal(props: IModal) {
    return <Container open={props.open}>{props.children}</Container>;
}
