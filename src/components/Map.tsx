import React, { useState, useEffect } from "react";
import GoogleMapReact, {
    Coords,
    ChangeEventValue,
    Bounds
} from "google-map-react";
import styled from "styled-components";
import firebase from "../api/firebase";
import { CurrentLocation, LocationPinIcon, Target } from "../common/icons";
import FilterIcon, { FilterType } from "../icons/filters/FilterIcon";
import MapStyle from "./MapStyle";
const StyledMap = styled(GoogleMapReact)`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
`;

const Pin = styled.div<{ lat: number; lng: number }>`
    width: 30px;
    height: 30px;
    cursor: pointer;
    & > svg {
        filter: drop-shadow(0px 0px 4px rgba(0, 0, 0, 0.3));
    }
`;

const LocationPin = styled.div<{ lat: number; lng: number }>`
    width: 15px;
    color: #ffb3ba;
`;

const Locate = styled.div`
    position: absolute;
    color: #bae1ff;
    width: 7vh;
    bottom: 1.5vh;
    right: 1.5vh;
    transition: all 150ms;
    transform: rotate(0deg);
    -webkit-filter: drop-shadow(0px 0px 5px rgba(0, 0, 0, 0.2));
    & > svg {
        filter: drop-shadow(0px 0px 4px rgba(0, 0, 0, 0.3));
    }
    :active {
        transform: scale(0.9) rotate(45deg);
    }
`;

const LocationMode = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    width: 60px;
    transform: translate3d(-50%, -50%, 0);
    color: #ffb3ba;
`;

const SetLocation = styled.div`
    position: absolute;
    bottom: 25%;
    left: 50%;
    padding: 1%;
    font-size: 1.25em;
    background: #ffdfba;
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.2);
    transform: translate3d(-50%, -50%, 0);
    color: #fff;
    :active {
        transform: scale(0.9) translate3d(-50%, -50%, 0);
    }
`;

export interface IPin {
    id: string;
    title: string;
    description: string;
    geopoint: firebase.firestore.GeoPoint;
    geohash: string;
    images: string[];
    filters: FilterType[];
}

// const getRange = (
//     nw: { lat: number; lng: number },
//     se: {
//         lat: number;
//         lng: number;
//     }
// ): { lower: string; upper: string } => {
//     return {
//         lower: Geohash.encode(se.lat, se.lng, 52),
//         upper: Geohash.encode(nw.lat, nw.lng, 52)
//     };
// };

function usePins(e?: Bounds) {
    const [pins, setPins] = useState<IPin[]>([]);
    useEffect(() => {
        const unsubscribe = firebase
            .firestore()
            .collection("Pins")

            .onSnapshot(snapshot => {
                const data = snapshot.docs.map(doc => {
                    return {
                        id: doc.id,
                        ...doc.data()
                    } as IPin;
                });
                setPins(data);
            });

        return () => unsubscribe();
    }, []);
    return pins;
}

export default function Map(props: {
    locationMode: boolean;
    setLocationMode: (_: boolean) => void;
    setCurrentCenter: (_: Coords) => void;
    setAddPinOpen: (_: boolean) => void;
    isMarkerModalOpen: boolean;
    setMarkerModalOpen: (_: boolean) => void;
    setVisibleMarkerData: (_: IPin | null) => void;
}) {
    const [currentZoom, setCurrentZoom] = useState<number>(7);
    const [currentBounds, setCurrentBounds] = useState<Bounds>();
    const [currentPosition, setCurrentPosition] = useState<Coords>();

    const setCurrentLocation = () => {
        if (!navigator.geolocation) {
            alert("Geolocation is not supported by your browser");
        } else {
            navigator.geolocation.getCurrentPosition(
                (r: Position) => {
                    setCurrentZoom(20);
                    setCurrentPosition({
                        lat: r.coords.latitude,
                        lng: r.coords.longitude
                    });
                },
                (error: PositionError) => console.log(error)
            );
        }
    };
    const pins = usePins(currentBounds);
    return (
        <>
            <StyledMap
                bootstrapURLKeys={{
                    key: `${process.env.REACT_APP_API_KEY ||
                        "AIzaSyAkrIILLDAQGkHXWzg20ob0AlCSYteGxPc"} `
                }}
                defaultCenter={{ lat: 54.9, lng: 9.9 }}
                defaultZoom={7}
                zoom={currentZoom}
                center={currentPosition}
                options={{
                    draggableCursor: "default",
                    draggingCursor: "move",
                    disableDefaultUI: true,
                    clickableIcons: false,
                    styles: MapStyle
                }}
                draggable
                onChange={(e: ChangeEventValue) => {
                    props.setMarkerModalOpen(false);
                    setCurrentZoom(e.zoom);
                    setCurrentBounds(e.bounds);
                    props.setVisibleMarkerData(null);
                    props.locationMode && props.setCurrentCenter(e.center);
                }}
            >
                {!props.locationMode &&
                    pins.map((p: IPin) => (
                        <Pin
                            onClick={() => {
                                props.setVisibleMarkerData(p);
                            }}
                            key={p.id}
                            lat={p.geopoint.latitude}
                            lng={p.geopoint.longitude}
                        >
                            {p.filters && p.filters[0] && (
                                <FilterIcon filter={p.filters[0]}></FilterIcon>
                            )}
                        </Pin>
                    ))}
                {currentPosition && (
                    <LocationPin
                        lat={currentPosition.lat}
                        lng={currentPosition.lng}
                    >
                        <LocationPinIcon></LocationPinIcon>
                    </LocationPin>
                )}
            </StyledMap>

            <Locate onClick={setCurrentLocation}>
                <CurrentLocation></CurrentLocation>
            </Locate>
            {props.locationMode && (
                <>
                    <LocationMode>
                        <Target />
                    </LocationMode>
                    <SetLocation
                        onClick={() => {
                            props.setAddPinOpen(true);
                            props.setLocationMode(false);
                        }}
                    >
                        set location
                    </SetLocation>
                </>
            )}
        </>
    );
}
