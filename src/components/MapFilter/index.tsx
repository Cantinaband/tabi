import React, { useState } from "react";
import styled from "styled-components";
import FilterIcon, { FilterType } from "../../icons/filters/FilterIcon";
import Filter from "./Filter";

const Scroll = styled.div`
    display: flex;
    align-items: center;
    margin: 0 -5vw;
    padding: 2% 5vw;
    pointer-events: all;
    flex-wrap: nowrap;
    overflow-x: scroll;
    overflow-y: hidden;
    justify-content: flex-start;
    align-items: center;
    ::-webkit-scrollbar {
        width: 0;
        height: 0;
        display: none;
    }
    -webkit-overflow-scrolling: touch; /* lets it scroll lazy */
`;

const FilterLabel = styled.span`
    font-size: 14px;
`;

interface Props {
    filters: FilterType[];
    onChange: (f: FilterType[]) => void;
}

const EndPlaceholder = styled.div`
    height: 10vh;
    min-width: 5vw;
`;

export default function MapFilters(props: Props) {
    const [filters, setFilters] = useState<FilterType[]>([]);

    const setFilter = (f: FilterType) => {
        const index = filters.findIndex(e => e === f);
        if (index === -1) {
            setFilters([...filters, f]);
            props.onChange([...filters, f]);
        } else {
            let _ = [...filters];
            _.splice(index, 1);
            setFilters(_);
            props.onChange(_);
        }
    };
    return (
        <Scroll>
            {props.filters &&
                props.filters.map((filter, i) => (
                    <Filter
                        key={i}
                        active={!!filters.find(e => e === filter)}
                        onClick={() => setFilter(filter)}
                    >
                        <FilterIcon filter={filter} key={i} />
                        <FilterLabel>{filter}</FilterLabel>
                    </Filter>
                ))}
            <EndPlaceholder />
        </Scroll>
    );
}
