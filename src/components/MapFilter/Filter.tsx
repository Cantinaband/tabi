import React, { ReactNode } from "react";
import styled, { css } from "styled-components";

interface IFilter {
    children: ReactNode;
    active: boolean;
    onClick: () => void;
}
const StyledFilter = styled.div<IFilter>`
    opacity: 0.5;
    min-width: 10vh;
    text-align: center;
    margin: 0 2%;
    ${p =>
        p.active &&
        css`
            transform: scale(1.15);
            opacity: 1;
        `}
    -webkit-tap-highlight-color: rgba(0,0,0,0);
`;

export default function Filter(props: IFilter) {
    return <StyledFilter {...props}>{props.children}</StyledFilter>;
}
