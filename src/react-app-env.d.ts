/// <reference types="react-scripts" />
declare namespace NodeJS {
    interface ProcessEnv {
        REACT_APP_API_KEY: string;
        FIREBASE_API: string;
        FIRESTORE: string;
        FIREBASE_PROJECT_ID: string;
        BUCKET: string;
        SENDER_ID: string;
        FIREBASE_APP_ID: string;
    }
}
