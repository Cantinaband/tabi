import React, { ReactNode } from "react";
import styled from "styled-components";

const StyledGrid = styled.div`
    pointer-events: none;
    position: absolute;
    top: 0;
    display: grid;
    height: 100%;
    width: 100%;
    grid-template-columns: 1fr;
    grid-template-rows: repeat(15, 1fr);
    grid-template-areas:
        "header"
        "filters"
        "filters"
        "."
        "."
        "."
        "."
        "."
        "."
        "."
        "."
        "."
        "."
        "."
        "footer";
`;

interface IProps {
    children: ReactNode[] | ReactNode;
}
export default function Grid(props: IProps) {
    return <StyledGrid>{props.children}</StyledGrid>;
}
