import React, { ReactNode } from "react";
import styled from "styled-components";

const StyledButton = styled.div`
    pointer-events: all;
    cursor: pointer;
`;

interface IButtonProps {
    children?: ReactNode[] | ReactNode;
    onClick: () => void;
    className?: string;
}
export default function Button(props: IButtonProps) {
    return (
        <StyledButton className={props.className} onClick={props.onClick}>
            {props.children}
        </StyledButton>
    );
}
