import React from "react";
import { CurrentLocation } from "./icons";
import Button from "./Button";
import styled from "styled-components";

const Location = styled(Button)`
    margin: 2%;
    pointer-events: all;
    width: 10vh;
`;

interface ILocationButton {
    onClick: () => void;
}

export default function LocationButton(props: ILocationButton) {
    return (
        <Location onClick={props.onClick}>
            <CurrentLocation />
        </Location>
    );
}
