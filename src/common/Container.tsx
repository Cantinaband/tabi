import styled from "styled-components";

/**
 * The main container, using for wrapping pages
 */
const Container = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100%;
`;

export default Container;
