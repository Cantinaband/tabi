import React from "react";

export const CurrentLocation = () => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 291.32 291.32">
            <path
                fill="#95b5cf"
                d="M145.66 0C65.547 0 0 65.547 0 145.66s65.547 145.66 145.66 145.66 145.66-65.547 145.66-145.66S225.772 0 145.66 0zm0 264.008c-65.547 0-118.348-52.802-118.348-118.348S80.113 27.311 145.66 27.311 264.008 80.113 264.008 145.66 211.206 264.008 145.66 264.008z"
            />
            <path
                fill="#e2574c"
                d="M221.22 70.099l-56.443 94.679-38.236-38.236 94.679-56.443z"
            />
            <path
                fill="#e4e7e7"
                d="M70.099 221.22l94.679-56.443-38.236-38.236-56.443 94.679z"
            />
        </svg>
    );
};

export const ModalOpen = () => {
    return (
        <svg version="1.0" viewBox="0 0 24 24">
            <path
                fill="currentColor"
                d="M12 2C6.5 2 2 6.5 2 12s4.5 10 10 10 10-4.5 10-10S17.5 2 12 2zm4.9 13.5l-1.4 1.4-3.5-3.5-3.5 3.5-1.4-1.4 3.5-3.5-3.5-3.5 1.4-1.4 3.5 3.5 3.5-3.5 1.4 1.4-3.5 3.5 3.5 3.5z"
            />
        </svg>
    );
};

export const LocationPinIcon = () => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            <path
                fill="currentColor"
                d="M256 0C153.755 0 70.573 83.182 70.573 185.426c0 126.888 165.939 313.167 173.004 321.035 6.636 7.391 18.222 7.378 24.846 0 7.065-7.868 173.004-194.147 173.004-321.035C441.425 83.182 358.244 0 256 0zm0 278.719c-51.442 0-93.292-41.851-93.292-93.293S204.559 92.134 256 92.134s93.291 41.851 93.291 93.293-41.85 93.292-93.291 93.292z"
            />
        </svg>
    );
};

export const Target = () => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60">
            <path
                fill="currentColor"
                d="M59 29h-2.025C56.458 14.907 45.093 3.542 31 3.025V1a1 1 0 10-2 0v2.025C14.907 3.542 3.542 14.907 3.025 29H1a1 1 0 100 2h2.025C3.542 45.093 14.907 56.458 29 56.975V59a1 1 0 102 0v-2.025C45.093 56.458 56.458 45.093 56.975 31H59a1 1 0 100-2zM31 54.975V53a1 1 0 10-2 0v1.975C16.01 54.46 5.54 43.99 5.025 31H7a1 1 0 100-2H5.025C5.54 16.01 16.01 5.54 29 5.025V7a1 1 0 102 0V5.025C43.99 5.54 54.46 16.01 54.975 29H53a1 1 0 100 2h1.975C54.46 43.99 43.99 54.46 31 54.975z"
            />
            <path
                fill="currentColor"
                d="M42 29h-5.08A7.005 7.005 0 0031 23.08V18a1 1 0 10-2 0v5.08A7.006 7.006 0 0023.08 29H18a1 1 0 100 2h5.08A7.005 7.005 0 0029 36.92V42a1 1 0 102 0v-5.08A7.006 7.006 0 0036.92 31H42a1 1 0 100-2z"
            />
        </svg>
    );
};
